<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');
use App\model\AuthorMaster;
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $auth= new AuthorMaster();
    $status = $auth->prepareData($_SESSION)->logged_in();
    if(!$status) {
        Utility::redirect('../signIn.php');
        return;
    }
}

else {
    Utility::redirect('../signIn.php');
}
use App\model\AuthorUpPaper;
$object=new AuthorUpPaper();
$object->prepareData($_GET);
$oneData = $object->showpaper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('../templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('authorNavigation.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="main-content"><!-- background not working -->
            <div class="col-md-12">
                <div class="box">
                    <?php

                    use App\Message\Message;


                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <p id='message' style='color: black; text-align: center; font-family: 'Times New Roman'; font-weight: 200 ;font-size: 20px;'><b>$msg</b></p>
                                </div>
                            </div>
                        </div>";
                    }

                    ?>
                    <div class="box-header header-vimeo">
                        <h2>Author panel</h2>
                    </div>
                    <div class="box-content">
                        <div class="row" style="min-height: 400px;">
                            <div class="col-md-8 col-md-offset-2" >
                                <div class="thumbnail">
                                    <object data="../../resources/papers/<?php echo $oneData->file_path?>" type="application/pdf" width="100%" height="600px">
                                    </object>
                                    <p style="text-align: center"><a href="../../resources/papers/<?php echo $oneData->file_path?>" target="_blank"><?php echo $oneData->topic_title?></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('../templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('../templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
