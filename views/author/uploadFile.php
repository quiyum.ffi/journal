<?php
session_start();
require_once("../../vendor/autoload.php");
require_once('../templateLayout/information.php');
use App\model\AuthorMaster;
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $auth= new AuthorMaster();
    $status = $auth->prepareData($_SESSION)->logged_in();
    if(!$status) {
        Utility::redirect('../signIn.php');
        return;
    }
}

else {
    Utility::redirect('../signIn.php');
}
$topic_category=new \App\model\TopicCategory();
$alldata=$topic_category->show();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('../templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('authorNavigation.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="main-content"><!-- background not working -->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header header-vimeo">
                        <h2>Upload File</h2>
                    </div>
                    <div class="box-content">
                        <div class="row" style="min-height: 400px;">
                            <?php

                            use App\Message\Message;


                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <p id='message' style='color: black; text-align: center; font-family: 'Times New Roman'; font-weight: 200 ;font-size: 20px;'><b>$msg</b></p>
                                </div>
                            </div>
                        </div>";
                            }

                            ?>
                            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3" style="margin-top: 80px">
                                <?php
                                ?>
                                <form class="form-horizontal" method="post" action="<?php echo base_url; ?>controller/uploadFile.php" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label class="control-label col-sm-3">Topic Category:</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="topic_category">
                                                <option value="selectOne">-Select any one-</option>
                                                <?php
                                                foreach ($alldata as $data){
                                                    echo "<option value=\"$data->id\">$data->name</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="email">Topic's Title:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" placeholder="Write your topic's title" name="title" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="email">Upload Your File:</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="email" placeholder="Enter Your email" name="file" accept="application/pdf" required>
                                        </div>
                                    </div>
                                    <input type="hidden" name="auth_id" value="<?php echo $_SESSION['id']?>"

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-info">Upload File</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('../templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('../templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
