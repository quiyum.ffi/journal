<?php
session_start();
require_once("../vendor/autoload.php");
require_once('templateLayout/information.php');

use App\model\AuthorUpPaper;
$object= new AuthorUpPaper();
$allData=$object->showAll();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('templateLayout/navigation.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="sidebar">
            <div class="col-md-12">
                <!---- Start Widget ---->
                <div class="widget wid-new-post" style="min-height: 500px;">
                    <div class="heading"><h4>Under Construction</h4></div>
                    <h1 style="text-align: center; line-height: 500px">This Page is under construction</h1>
                </div>
                <!---- Start Widget ---->
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
