<?php
session_start();
require_once("../vendor/autoload.php");
require_once('templateLayout/information.php');
use App\model\Notice;
$object=new Notice();
$allData=$object->show();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('templateLayout/templateCSS.php');?>
    <?php require_once('templateLayout/tableCss.php');?>
</head>

<body>
<header>
    <?php require_once('templateLayout/navigation.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="sidebar">
            <div class="col-md-12">
                <!---- Start Widget ---->
                <div class="widget wid-new-post" style="min-height: 500px;">
                    <div class="heading"><h4>Notice</h4></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            All Notice
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th style='text-align: center; width: 10%'>Serial</th>
                                        <th style='text-align: center; width: 20%'>Date</th>
                                        <th style='text-align: center; width: 50%'>Notice</th>
                                        <th style='text-align: center; width: 20%'>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $serial= 1;
                                    foreach($allData as $oneData){
                                        $date=date('d-m-Y', strtotime($oneData->date));
                                        echo" <tr>
                                    <td style='text-align: center'>$serial</td>
                                    <td style='text-align: center'>$date</td>
                                    <td style='text-align: center'>$oneData->title</td>
                                  
                                    <td style='text-align: center'>
                                         <a href='singleNotice.php?id=$oneData->id' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                    </td>
                                </tr>";
                                        $serial++;
                                    }?>



                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>

                </div>
                <!---- Start Widget ---->
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('templateLayout/templateScript.php');?>
<?php require_once('templateLayout/tableScript.php');?>
<!--script-->
</body>
</html>
