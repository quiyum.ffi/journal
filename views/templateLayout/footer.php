<footer>
    <div class="wrap-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-footer footer-1">
                    <div class="footer-heading"><h1><span style="color: #fff;">Online Conference Management System</span></h1></div>
                    <div class="content">
                        <p>Never missed any post published in our site. Subscribe to our daly newsletter now.</p>
                        <strong>Email address:</strong>
                        <form action="#" method="post">
                            <input type="text" name="your-name" value="" size="40" placeholder="Your Email" />
                            <input type="submit" value="SUBSCRIBE" class="btn btn-3" />
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-footer footer-3">
                    <div class="footer-heading"><h4>Link List</h4></div>
                    <div class="content">
                        <ul>
                            <li><a href="#">MOST VISITED COUNTRIES</a></li>
                            <li><a href="#">5 PLACES THAT MAKE A GREAT HOLIDAY</a></li>
                            <li><a href="#">PEBBLE TIME STEEL IS ON TRACK TO SHIP IN JULY</a></li>
                            <li><a href="#">STARTUP COMPANY’S CO-FOUNDER TALKS ON HIS NEW PRODUCT</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <p>Copyright 2017 - <a href="#" target="_blank" rel="nofollow">Nayan Talukder</a> </p>
    </div>
</footer>