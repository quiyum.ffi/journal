
<link rel="stylesheet" href="<?php echo base_url; ?>resources/css/bootstrap.min.css"  type="text/css">

<!-- Owl Carousel Assets -->
<link href="<?php echo base_url; ?>resources/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url; ?>resources/owl-carousel/owl.theme.css" rel="stylesheet">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url; ?>resources/css/style.css">
<link href="<?php echo base_url; ?>resources/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<!-- Custom Fonts -->
<link rel="stylesheet" href="<?php echo base_url; ?>resources/font-awesome-4.4.0/css/font-awesome.min.css"  type="text/css">

<!-- jQuery and Modernizr-->
<script src="<?php echo base_url; ?>resources/js/jquery-2.1.1.js"></script>

<!-- Core JavaScript Files -->
<script src="<?php echo base_url; ?>resources/js/bootstrap.min.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="<?php echo base_url; ?>resources/js/html5shiv.js"></script>
<script src="<?php echo base_url; ?>resources/js/respond.min.js"></script>
<![endif]-->