<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');
use App\model\ReviewerActivity;
use App\model\AuthorMaster;
use App\Utility\Utility;
if($_SESSION['role_status']==2){
    $auth= new AuthorMaster();
    $status = $auth->prepareData($_SESSION)->logged_in();
    if(!$status) {
        Utility::redirect('../signIn.php');
        return;
    }
}

else {
    Utility::redirect('../signIn.php');
}
$object=new ReviewerActivity();
$allData=$object->less_rating();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('../templateLayout/templateCSS.php');?>

    <?php require_once('../templateLayout/tableCss.php');?>

</head>

<body>
<header>
    <?php require_once('admin_menu.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="main-content"><!-- background not working -->
            <div class="col-md-12">
                <div class="box">
                    <?php

                    use App\Message\Message;


                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <p id='message' style='color: black; text-align: center; font-family: 'Times New Roman'; font-weight: 200 ;font-size: 20px;'><b>$msg</b></p>
                                </div>
                            </div>
                        </div>";
                    }

                    ?>
                    <div class="box-header header-vimeo">
                        <h2>Less Rated Papers </h2>
                    </div>
                    <div class="box-content">
                        <div class="row" style="min-height: 400px;">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        All less rated paper 
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="dataTable_wrapper">
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                <tr>
                                                    <th style='text-align: center'>Serial</th>
                                                    <th style='text-align: center'>Topic Title</th>
                                                    <th style='text-align: center'>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $serial= 1;
                                                foreach($allData as $oneData){
                                                    echo" <tr>
                                    <td style='text-align: center'>$serial</td>
                                    <td style='text-align: center'>$oneData->topic_title</td>
                                    <td style='text-align: center'>
                                         <a href='viewLessRatedPaper.php?id=$oneData->id' class='btn btn-info'><i class='fa fa-external-link-square ' aria-hidden='true'></i></a>
                                    </td>
                                </tr>";
                                                    $serial++;
                                                }?>



                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('../templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('../templateLayout/templateScript.php');?>
<?php require_once('../templateLayout/tableScript.php');?>
<!--script-->
</body>
</html>
