<?php
session_start();
require_once("../../vendor/autoload.php");
require_once('../templateLayout/information.php');
use App\model\Notice;
use App\model\AuthorMaster;
use App\Utility\Utility;
if($_SESSION['role_status']==2){
    $auth= new AuthorMaster();
    $status = $auth->prepareData($_SESSION)->logged_in();
    if(!$status) {
        Utility::redirect('../signIn.php');
        return;
    }
}

else {
    Utility::redirect('../signIn.php');
}
$object=new Notice();
$object->prepareData($_GET);
$oneData=$object->showNotice();
$originalDate = $oneData->con_date;
$newDate = date("d-m-Y", strtotime($originalDate));
$beforDate=date('Y-m-d', strtotime($originalDate.'-10 days'));
$submitPaperDate=date("d-m-Y", strtotime($beforDate));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('../templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('admin_menu.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="sidebar">
            <div class="col-md-12">
                <!---- Start Widget ---->
                <div class="widget wid-new-post" style="min-height: 500px;">
                    <div class="heading"><h4>Notice</h4></div>
                        <h4 style="text-align: center"><?php echo $oneData->topic?></h4>
                        <p style="text-align: center"><b>Date: </b><?php echo date("d-m-Y", strtotime($oneData->date))?> </p>
                    <hr>
                        <div class="col-md-8 col-md-offset-2" style="text-align: center">
                            <p><b>Notice Title: </b><?php echo $oneData->title?> </p>
                            <p><b>Conference Date: </b><?php echo $newDate?> </p>
                            <p><b>Last Date of Paper Submission on topics "<?php echo $oneData->topic?>" : </b><?php echo $submitPaperDate?> </p>
                            <p><b>Conference Place: </b><?php echo $oneData->place?> </p>
                            <p><b>Description: </b><?php echo $oneData->description?> </p>

                        </div>

                </div>
                <!---- Start Widget ---->
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('../templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('../templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
