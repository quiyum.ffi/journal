
<!--Top-->
<nav id="top">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <strong>Welcome to Us!</strong>
            </div>
            <div class="col-md-6">
                <ul class="list-inline top-link link">
                    <li><a href="<?php echo base_url; ?>views/admin/index.php"><i class="fa fa-home"></i> Home</a></li>
                    <li><a href="<?php echo base_url; ?>views/admin/contact.php"><i class="fa fa-comments"></i> Cantact</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<!--Navigation-->
<nav id="menu" class="navbar container">
    <div class="navbar-header">
        <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
        <a class="navbar-brand" href="#">
            <div class="logo"><span>OCMS</span></div>
        </a>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url; ?>views/admin/index.php">Home</a></li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Journals by Subject <i class="fa fa-arrow-circle-o-down"></i></a>
                <div class="dropdown-menu">
                    <div class="dropdown-inner">
                        <ul class="list-unstyled">
                            <li><a href="<?php echo base_url; ?>views/admin/artsAndHumities.php">Arts & Humanities </a></li>
                            <li><a href="<?php echo base_url; ?>views/admin/law.php">Law </a></li>
                            <li><a href="<?php echo base_url; ?>views/admin/medical.php">Medicine & Health</a></li>
                            <li><a href="<?php echo base_url; ?>views/admin/science.php">Science & Mathematics</a></li>
                            <li><a href="<?php echo base_url; ?>views/admin/social.php">Social Sicence </a></li>

                        </ul>
                    </div>
                </div>
            </li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Papers <i class="fa fa-arrow-circle-o-down"></i></a>
                <div class="dropdown-menu">
                    <div class="dropdown-inner">
                        <ul class="list-unstyled">

                            <li><a href="<?php echo base_url; ?>views/admin/lessRated.php"> Less Rated Papers</a></li>
                            <li><a href="<?php echo base_url; ?>views/admin/uploadedPaper.php">New Uploaded Paper</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Notice <i class="fa fa-arrow-circle-o-down"></i></a>
                <div class="dropdown-menu">
                    <div class="dropdown-inner">
                        <ul class="list-unstyled">
                            <li><a href="<?php echo base_url; ?>views/admin/notice.php">Published Notice</a></li>
                            <li><a href="<?php echo base_url; ?>views/notice/create.php"> Create New Notice</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li><a href="<?php echo base_url?>views/admin/assignReviewer.php"><i class="fa fa-cubes"></i>Assign Reviewer</a></li>
            <li><a href="<?php echo base_url?>controller/logout.php"><i class="fa fa-cubes"></i>Log Out</a></li>
        </ul>
        <ul class="list-inline navbar-right top-social">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
        </ul>
    </div>
</nav>