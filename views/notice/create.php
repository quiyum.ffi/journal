<?php
session_start();
require_once("../../vendor/autoload.php");
require_once('../templateLayout/information.php');

use App\model\TopicCategory;
use App\model\AdminMaster;
use App\model\AuthorMaster;
use App\Utility\Utility;


if($_SESSION['role_status']==2){
    $auth= new AuthorMaster();
    $status = $auth->prepareData($_SESSION)->logged_in();
    if(!$status) {
        Utility::redirect('../signIn.php');
        return;
    }
}

else {
    Utility::redirect('../signIn.php');
}
$topicscategory=new TopicCategory();
$allcategory=$topicscategory->show();

$adminmaster=new AdminMaster();
$alladmin=$adminmaster->show();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
    <meta name="author" content="http://www.365bootstrap.com">

    <title><?php echo $title?></title>

    <!-- Bootstrap Core CSS -->
    <?php require_once('../templateLayout/templateCSS.php');?>
</head>

<body>
<header>
    <?php require_once('../admin/admin_menu.php');?>
</header>


<!-- /////////////////////////////////////////Content -->
<div id="page-content" class="index-page container">
    <div class="row">
        <div id="main-content"><!-- background not working -->
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header header-vimeo">
                        <h2>Notice</h2>
                    </div>
                    <div class="box-content">
                        <div class="row" style="min-height: 400px;">
                            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3" style="margin-top: 30px">
                                <form class="form-horizontal" action="<?php echo base_url; ?>controller/addnoticeController.php" method="post">

                                    <div class="form-group">
                                        <label class="control-label col-sm-3">Category:</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="cat_id">
                                                <option value="select">-Select any one-</option>
                                                <?php foreach ($allcategory as $category){ ?>
                                                <option value=<?php echo $category->id?> ><?php echo $category->name?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="email">Title:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="email" placeholder="write Titile" name="title" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="pwd">Description:</label>
                                        <div class="col-sm-9">
                                                <textarea class="form-control" rows="5" id="comment" name="description"></textarea>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="email">Conference Date:</label>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="email" placeholder="Date" name="date" max="31" required>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="email" placeholder="Month" name="month" max="12" required>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="email" placeholder="Year" name="year" min="2017" required>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="email">Place of Conference:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="email" placeholder="Full address of conference hall" name="place" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-info">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php require_once('../templateLayout/footer.php');?>
<!-- Footer -->
<!--script-->
<?php require_once('../templateLayout/templateScript.php');?>
<!--script-->
</body>
</html>
