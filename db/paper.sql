-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 12, 2017 at 11:44 PM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paper`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_details`
--

CREATE TABLE `admin_details` (
  `master_id` int(11) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `mobile` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_master`
--

CREATE TABLE `admin_master` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_master`
--

INSERT INTO `admin_master` (`id`, `name`, `email`, `pass`) VALUES
(1, 'Nayan Talukdar', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'Forhad', 'forhad@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `authors_details`
--

CREATE TABLE `authors_details` (
  `master_id` int(11) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `mobile` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `authors_master`
--

CREATE TABLE `authors_master` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authors_master`
--

INSERT INTO `authors_master` (`id`, `name`, `email`, `pass`) VALUES
(9, 'Mr. A', 'a@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(10, 'Mr X', 'author@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(11, 'Farhan', 'abc@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(12, 'jyati', 'jyati@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `authors_up_paper`
--

CREATE TABLE `authors_up_paper` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `topic_cat_id` int(11) NOT NULL,
  `topic_title` varchar(200) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `status` varchar(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authors_up_paper`
--

INSERT INTO `authors_up_paper` (`id`, `author_id`, `file_path`, `topic_cat_id`, `topic_title`, `date`, `status`) VALUES
(3, 9, '1494534929157 A DIBP Application form .pdf', 1, 'Application for a student visa', '2017-05-12', '1'),
(4, 9, '1494542630Job Leave Approval .pdf', 2, 'Crime', '2017-05-12', '1'),
(5, 9, '1494542725Job Leave Application .pdf', 1, 'Arts', '2017-05-12', '1'),
(6, 9, '14945432681277-Mr A.pdf', 1, 'Arts', '2017-05-12', 'rejected'),
(7, 10, '1494577374157 A DIBP Application form .pdf', 1, 'Demo Paper', '2017-05-12', '1'),
(8, 11, '1494579108Job Leave Application .pdf', 4, 'Newton\'s Law', '2017-05-12', '1'),
(9, 9, '1494580862Job Appointment.pdf', 5, 'Social Thinking', '2017-05-12', '1'),
(10, 9, '1494581708asdf.pdf', 3, 'Health', '2017-05-12', '1');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `admin_id` int(11) NOT NULL,
  `topic_cat_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `conference_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `conference_place` varchar(200) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `date`, `admin_id`, `topic_cat_id`, `title`, `description`, `conference_date`, `conference_place`, `status`) VALUES
(1, '2017-05-13', 1, 3, 'Conference will be held on Medicine ', 'Conference will be held on Medicine. Paper must be submit on 4th June.', '2017-06-09 18:00:00', 'G.E.C. Convention Hall, Chittagong', '0'),
(2, '2017-05-13', 1, 1, 'Fifteenth International Conference on  New Directions in the Humanities', 'The Fifteenth International Conference on New Directions in the Humanities features research addressing the following annual themes and the 2017 Special Focus.', '2017-06-19 18:00:00', 'G.E.C. Convention Hall, Chittagong', '0');

-- --------------------------------------------------------

--
-- Table structure for table `reviewer_activity`
--

CREATE TABLE `reviewer_activity` (
  `id` int(11) NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `athors_up_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviewer_activity`
--

INSERT INTO `reviewer_activity` (`id`, `reviewer_id`, `athors_up_id`, `comments`, `rating`) VALUES
(1, 5, 3, 'This is awesome', 5),
(2, 5, 3, 'This is awesome', 5),
(3, 6, 3, 'This is fantastic', 4),
(4, 6, 5, 'Its good', 4),
(5, 5, 3, 'Informative ', 5),
(6, 6, 3, 'not good', 1),
(7, 7, 3, 'Its fantastic', 5),
(8, 8, 7, 'This paper is awesome and informative', 5),
(9, 8, 4, 'Better', 4),
(10, 8, 9, 'gfgfgf', 5),
(11, 8, 9, 'vcvcvcvcv', 5),
(12, 8, 9, ' cvcvcvcvcvc', 3),
(13, 8, 9, 'sdsdsds', 5);

-- --------------------------------------------------------

--
-- Table structure for table `reviewer_details`
--

CREATE TABLE `reviewer_details` (
  `master_id` int(11) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `mobile` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviewer_master`
--

CREATE TABLE `reviewer_master` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviewer_master`
--

INSERT INTO `reviewer_master` (`id`, `name`, `email`, `pass`) VALUES
(4, 'Farhan', 'ahbabul66666@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(5, 'Nayan Talukdar', 'reviewer@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(6, 'Amit Hasan', 'a@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(7, 'Hasan', 'hasan@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(8, 'Nayan Talukdar', 'nayan@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(9, 'nayan jyati talulkder', 'nayan.cmt1@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `topic_category`
--

CREATE TABLE `topic_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topic_category`
--

INSERT INTO `topic_category` (`id`, `name`) VALUES
(1, 'Arts & Humanities'),
(2, 'Law'),
(3, 'Medicine & Health'),
(4, 'Sicence & Mathmatics'),
(5, 'Social Sicence');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_master`
--
ALTER TABLE `admin_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authors_master`
--
ALTER TABLE `authors_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authors_up_paper`
--
ALTER TABLE `authors_up_paper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviewer_activity`
--
ALTER TABLE `reviewer_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviewer_master`
--
ALTER TABLE `reviewer_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic_category`
--
ALTER TABLE `topic_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_master`
--
ALTER TABLE `admin_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `authors_master`
--
ALTER TABLE `authors_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `authors_up_paper`
--
ALTER TABLE `authors_up_paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reviewer_activity`
--
ALTER TABLE `reviewer_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `reviewer_master`
--
ALTER TABLE `reviewer_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `topic_category`
--
ALTER TABLE `topic_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
