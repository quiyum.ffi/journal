<?php
/**
 * Created by PhpStorm.
 * User: tusar.hasan
 * Date: 5/10/2017
 * Time: 11:21 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use PDO;
use  PDOException;
class AdminMaster extends Database
{
    public $id;
    public $email="";
    public $pass="";


    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->pass = md5($data['password']);
        }
        return $this;
        
    }
    public function store(){

        $query= "INSERT INTO delivery_master (retailer_name,retailer_shop_name,	retailer_address,contact_no,order_date,delivery_date,total_payment) VALUES (?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$givedata);
        $STH->bindParam(1,$givedata);
        $STH->bindParam(1,$givedata);

        $result = $STH->execute();

    }
    public function update(){
        $query= 'UPDATE delivery_master SET status = ? WHERE id=?';

        $STH = $this->DBH->prepare($query);
        
        $STH->bindParam(1,$givedata);
        
        $result = $STH->execute();

    }
    public function show(){
        
        $sql = "SELECT * FROM `admin_master`";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function delete(){

    }
    public function getMasterid(){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function viewSingleRow($id){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function loginCheck(){
        $query = "SELECT * FROM `admin_master` WHERE `email`='$this->email' AND `pass`='$this->pass'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function logged_in(){
        if ((array_key_exists('email', $_SESSION)) && (!empty($_SESSION['email']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }
    public function viewId(){
        $sql = "SELECT id as userId FROM `admin_master` WHERE `email`='$this->email'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
}