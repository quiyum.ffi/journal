<?php
/**
 * Created by PhpStorm.
 * User: tusar.hasan
 * Date: 5/10/2017
 * Time: 11:27 PM
 */

namespace App\model;
use App\database\Database;
use PDO;

class Notice extends Database
{
    public $id;
    public $date;
    public $description;
    public $conference_date;
    public $admin_id;
    public $place;
    public $post_by_id;
    public $status;
    public $topic_cat_id;
    public $title;


    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('cat_id', $data)) {
            $this->topic_cat_id = ($data['cat_id']);
        }
        if (array_key_exists('admin_id', $data)) {
            $this->admin_id = $data['admin_id'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('confer_date', $data)) {
            $this->conference_date = $data['confer_date'];
        }
        if (array_key_exists('place', $data)) {
            $this->place = $data['place'];
        }
        return $this;

    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO notice (date,admin_id,topic_cat_id,title,description,conference_date,conference_place) VALUES (?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->date);
        $STH->bindParam(2,$this->admin_id);
        $STH->bindParam(3,$this->topic_cat_id);
        $STH->bindParam(4,$this->title);
        $STH->bindParam(5,$this->description);
        $STH->bindParam(6,$this->conference_date);
        $STH->bindParam(7,$this->place);

        $result = $STH->execute();

    }
    public function update(){
        $query= 'UPDATE delivery_master SET status = ? WHERE id=?';

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$givedata);

        $result = $STH->execute();

    }
    public function show(){

        $sql = "SELECT * FROM notice WHERE status='0' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function showNotice(){
        $sql = "SELECT topic_category.name as topic, notice.date as date,notice.title as title,notice.description as description, notice.conference_date as con_date, notice.conference_place as place FROM notice,topic_category WHERE status='0' and notice.id='$this->id' and notice.topic_cat_id=topic_category.id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
    public function delete(){

    }
    public function getMasterid(){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function viewSingleRow($id){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
}