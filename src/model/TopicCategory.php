<?php
/**
 * Created by PhpStorm.
 * User: tusar.hasan
 * Date: 5/10/2017
 * Time: 11:30 PM
 */

namespace App\model;
use App\database\Database;
use PDO;

class TopicCategory extends Database
{
    public $id;
    public $name;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('topics_category_name', $data)) {
            $this->name= $data['topics_category_name'];
        }
        return $this;

    }
    public function store(){

        $query= "INSERT INTO `topic_category` (`name`) VALUES (?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->name);


        $result = $STH->execute();

    }
    public function update(){
        $query= 'UPDATE delivery_master SET status = ? WHERE id=?';

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$givedata);

        $result = $STH->execute();

    }
    public function show(){

        $sql = "SELECT * FROM `topic_category`";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function delete(){

    }
    public function getMasterid(){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function viewSingleRow($id){
        $sql = "";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
}